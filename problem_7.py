# ---------------------------------------------------------------------------------------------------------------------
# Project:       math (https://projecteuler.net)
# Description:   Problem_7
# Version:       1.0.1  --  Python 3.7
# Author:        Stanislav Fux  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

"""
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13,
we can see that the 6th prime is 13.
What is the 10 001st prime number?
"""


def eratosthenes():
    a = {}
    b = 2
    while 1:
        if b not in a:
            yield b
            a[b * b] = [b]
        else:
            for c in a[b]:
                a.setdefault(c + b, []).append(c)
            del a[b]
        b = b + 1


def prime_number(n):
    for i, prime in enumerate(eratosthenes()):
        if i == n - 1:
            return prime


print(prime_number(10001))

# [DONE]
