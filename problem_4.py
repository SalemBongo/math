# ---------------------------------------------------------------------------------------------------------------------
# Project:       math (https://projecteuler.net)
# Description:   Problem_4
# Version:       1.0.1  --  Python 3.7
# Author:        Stanislav Fux  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

"""
A palindromic number reads the same both ways.
The largest palindrome made from
the product of two 2-digit numbers is 9009 = 91 × 99.
Find the largest palindrome made from the product of two 3-digit numbers.
"""


def palindrome():
    result = []
    for i in range(100, 1000):
        for v in range(100, 1000):
            b = str(i * v)
            c = b[::-1]
            if c == b:
                result.append(int(b))
    return max(result)


print(palindrome())

# [DONE]
