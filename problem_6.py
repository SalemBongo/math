# ---------------------------------------------------------------------------------------------------------------------
# Project:       math (https://projecteuler.net)
# Description:   Problem_6
# Version:       1.0.1  --  Python 3.7
# Author:        Stanislav Fux  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

"""
The sum of the squares of the first ten natural numbers is,
12 + 22 + ... + 102 = 385
The square of the sum of the first ten natural numbers is,
(1 + 2 + ... + 10)2 = 552 = 3025
Hence the difference between the sum of the squares of the first ten natural
numbers and the square of the sum is 3025 − 385 = 2640.
Find the difference between the sum of the squares of the first one hundred
natural numbers and the square of the sum.
"""


def diff():
    a = range(1, 101)
    b = sum(a)
    c = b * b - sum(i * i for i in a)
    return c


print(diff())

# [DONE]
