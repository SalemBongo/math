# ---------------------------------------------------------------------------------------------------------------------
# Project:       math (https://projecteuler.net)
# Description:   Problem_3
# Version:       1.1.1  --  Python 3.7
# Author:        Stanislav Fux  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fux, 2018
# ---------------------------------------------------------------------------------------------------------------------

"""
The prime factors of 13195 are 5, 7, 13 and 29.
What is the largest prime factor of the number 600851475143 ?
"""


def factors(n):
    while n > 1:
        for i in range(2, n + 1):
            if n % i == 0:
                n = n // i
                yield i
                break


for f in factors(600851475143):
    print(f)

# [DONE]
